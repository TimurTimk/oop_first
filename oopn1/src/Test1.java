public class Test1 {
    private int number;
    private int number2;
    public Test1(int number, int number2){
        this.number=number;
        this.number=number2;

    }

    public int getNumber() {
        return number;
    }

    public int getNumber2() {
        return number2;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setNumber2(int number2) {
        this.number2 = number2;
    }

    public int countSum() {
        return this.number2 + this.number;
    }

    public int countMax() {
        return Math.max(this.number, this.number2);
    }

}
