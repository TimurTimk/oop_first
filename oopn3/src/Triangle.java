public class Triangle {
    private final int sideA;
    private final int sideB;
    private final int sideC;
    public Triangle(int sideA, int sideB, int sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    public int getSideA() {return sideA;}
    public int getSideB() {
        return sideB;
    }
    public int getSideC() {
        return sideC;
    }
    public  int perimeter() {
        return this.sideA + this.sideB + this.sideC;
    }
    public  double area() {
        int p = perimeter() / 2;
        return Math.sqrt(p * (p - this.sideA) * (p - this.sideB) * (p - this.sideC));
    }
}
