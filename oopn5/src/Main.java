public class Main {
    public static void main(String[] args) {
        TimeN5 timeFirst = new TimeN5(54,23,5);
        System.out.println("Hours- "+timeFirst.getHours());
        System.out.println("Minutes- "+timeFirst.getMinutes());
        System.out.println("Seconds- "+timeFirst.getSeconds());
        timeFirst.increaseHours(3);
        timeFirst.increaseMinutes(20);
        timeFirst.increaseSeconds(56);
        System.out.println();
        System.out.println("Hours- "+timeFirst.getHours());
        System.out.println("Minutes- "+timeFirst.getMinutes());
        System.out.println("Seconds- "+timeFirst.getSeconds());

    }
}