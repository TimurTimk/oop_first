public class TimeN5 {
    private int hours;
    private int minutes;
    private int seconds;
    public int getHours() {
        return hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public TimeN5(int hours, int minutes, int seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }


    public void increaseHours (int value){
        hours+=value;
        if(hours>60){
            hours=0;
        }
    }
    public void increaseMinutes (int value){
        minutes+=value;
        if(minutes>60){
            minutes=0;
        }
    }

    public void increaseSeconds (int value){

        seconds+=value;
        if (seconds>60){
            seconds=0;
        }
    }
}
