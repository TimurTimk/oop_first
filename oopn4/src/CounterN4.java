public class CounterN4 {
    private int value;
    private int boundMin;
    private int boundMax;


    public int getValue() {
        return value;
    }

        public int setValue(int value) {
        this.value = value;
        return value;
    }

    public void setBoundFirstBoundSecond(int boundFirst, int boundSecond) {
        this.boundMax = boundSecond;
        this.boundMin = boundFirst;
    }

    public int getBoundMin() {
        return boundMin;
    }

    public int getBoundMax() {
        return boundMax;
    }

    public CounterN4(int value, int boundFirst, int boundSecond) {
        this.value = value;
        this.boundMin = boundFirst;
        this.boundMax = boundSecond;

    }

    public int increase(int newValue) {
        if (boundMin > value + newValue) {
            System.out.println("the value of the counter goes beyond its boundaries");
        }
        return value + newValue;
    }

    public int decrease(int newValue) {
        if (value - newValue < boundMax) {
            System.out.println("the value of the counter goes beyond its boundaries");
        }
        return value - newValue;
    }
}
